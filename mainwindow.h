#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "serialdisplay.h"
#include <QMainWindow>
#include <QTimer>
#include <QVector2D>
#include <Qt>
#include <random>

namespace Ui
{
class MainWindow;
}

/**
 * @brief Trida hlavnoho okna
 * @details V této třídě je implementováno hlavní okno programu.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    /**
     * @brief Udalost tlacitka start
     * @details Tato metoda otevre port vybrany v Ui::serialComboBox a resetuje
     * displej.
     */
    void on_pushButton_clicked();

    /**
     * @brief Udalost tlacitka stop
     * @details Tato metoda zastavi smycku hry a uzavre port;
     */
    void on_pushButton_2_clicked();

    /**
     * @brief Udalost hlavniho casovace
     * @details Tato událost se volá cyklicky každých 50ms a přepočítá pozice
     * prvnů ve hře.
     */
    void on_timer_timeout();

    /**
     * @brief Událost zrychlujícího čítače
     * @details Tato událost se volá každou sekundu a zrychlí pohyb balonku 1.01
     * krát.
     */
    void on_speeduptimer_timeout();

private:
    /**
     * @brief Udalost na odchytavani stisku klaves
     * @param key stisknuta klavesa
     */
    void keyPressEvent(QKeyEvent *key) override;

    /**
     * @brief Událost na odchytávání uvolnění kláves
     * @param key uvolněná klávesa
     */
    void keyReleaseEvent(QKeyEvent *key) override;

    /**
     * @brief Generátor počátečního směru balónku
     * @details Využívá std::uniform_real_distribution. Směr generuje tak aby
     * balonek byl odchýlen maximálně 45° od horizonu.
     */
    void generate_ball_direction();

    /**
     * @brief Reset hry
     * @details Nastaví obě pálky, balonek a skóre na počáteční hodnoty.
     */
    void reset_pos();

    /**
     * @brief Vykreslení startovní obrazovky
     */
    void draw_reset();

    /**
     * @brief Výpočet pozice levé pálky
     */
    void calculate_left_bat();

    /**
     * @brief Vykreslení levé pálky
     * @details Pokud se pálka nepohnula nic se nekreslí, jinak zavolá
     * MainWindow::clear_left_bat a MainWindow::redraw_left_bat.
     */
    void draw_left_bat();

    /**
     * @brief Smazání levé pálky
     * @details Nakreslí na původní pozici pálky černý pruh
     */
    void clear_left_bat();

    /**
     * @brief Překreslí pálku
     * @details Nakreslí pálku na nové místo
     */
    void redraw_left_bat();

    /**
     * @brief Výpočet pozice pravé pálky
     */
    void calculate_right_bat();

    /**
     * @brief Vykreslení pravé pálky
     * @details Pokud se pálka nepohnula nic se nekreslí, jinak zavolá
     * MainWindow::clear_right_bat a MainWindow::redraw_right_bat.
     */
    void draw_right_bat();

    /**
     * @brief Překreslí pálku
     * @details Nakreslí pálku na nové místo
     */
    void clear_right_bat();

    /**
     * @brief Překreslí pálku
     * @details Nakreslí pálku na nové místo
     */
    void redraw_right_bat();

    /**
     * @brief Vykreslí balonek a vypočítá jeho novou pozici
     *
     */
    void draw_ball();

    /**
     * @brief Vykreslí skóre
     * @details Pokud se skóre nezměnilo nic se nekreslí, jinak zavolá
     * MainWindow::clear_score a MainWindow::redraw_score
     */
    void draw_score();

    /**
     * @brief Smaže skóre
     * @details NAkreslí na místo skóre černý pruh
     */
    void clear_score();

    /**
     * @brief překreslí skóre
     * @details Nakreslí nové skóre
     */
    void redraw_score();

    /**
     * @brief otestuje zda balónek překrývá pravou pálku
     * @return výsledek
     */
    bool ball_right_bat_intersection();

    /**
     * @brief otestuje zda balónek překrývá levou pálku
     * @return výsledek
     */
    bool ball_left_bat_intersection();

    /**
     * @brief otestuje zda balónek překrývá skóre
     * @return výsledek
     */
    bool ball_score_intersection();

    Ui::MainWindow *ui;

    /**
     * @brief časovač hlavní smyčky
     * @details vyvolává MainWindow::on_timer_timeout
     */
    QTimer timer;

    /**
     * @brief časovač zrychlování balónku
     * @details vyvolává MainWindow::on_speeduptimer_timeout
     */
    QTimer speedUpTimer;

    /**
     * @brief pozice balonku
     */
    QVector2D ballPosition;

    /**
     * @brief směrový vektor balonku
     */
    QVector2D ballDirection;

    /**
     * @brief pozice levé pálky
     */
    int leftBatPosition;

    /**
     * @brief pozice pravé pálky
     */
    int rightBatPosition;

    /**
     * @brief pozice balónku v minulém snímku
     */
    QVector2D ballPosition_prev;

    /**
     * @brief pozice levé pálky v minulém snímku
     */
    int leftBatPosition_prev;

    /**
     * @brief pozice pravé pálky v minulém snímku
     */
    int rightBatPosition_prev;

    /**
     * @brief rozhraní pro komunikaci s displejem
     */
    serialDisplay display;

    /**
     * @brief indikuje stisk klávesy Q
     */
    bool qKey = false;

    /**
     * @brief indikuje stisk klávesy A
     */
    bool aKey = false;

    /**
     * @brief indikuje stisk klávesy P
     */
    bool pKey = false;

    /**
     * @brief indikuje stisk klávesy L
     */
    bool lKey = false;

    /**
     * @brief indikuje stisk mezerníku
     */
    bool spaceKey = false;

    /**
     * @brief Generátor náhodných čísel pro MainWindow::generate_ball_direction
     */
    std::default_random_engine generator;

    /**
     * @brief Distribuce náhodných čísel pro MainWindow::generate_ball_direction
     */
    std::uniform_real_distribution<float> distribution{-1, 1};

    /**
     * @brief skóre levého hráče
     */
    quint8 leftScore = 0;

    /**
     * @brief skóre pravého hráče
     */
    quint8 rightScore = 0;

    /**
     * @brief naformátované skóre
     */
    QString string;

    /**
     * @brief naformátované skóre z předchozího snímku
     */
    QString string_prev;

    /**
     * @brief indikuje zda hra běží
     */
    bool running = false;
};

#endif // MAINWINDOW_H

# Pingpong {#MainPage}

Hra pingpong naprogramovaná pro zankový displej připojený sériovým portem.

## Ovládání

V nabídce se vybere příslušný port a stiskne tlačítko start.
Prvky se nastaví do výchozích poloh.
Stiskem mezerníku se vyšle balónek, který se vydá náhodným směrem.
Levý hráč ovládá pálku klávesami `Q` a `A`, pravý hráč ovládá svou pálku klávesami `P` a `L`.

## Pravidla

Oba hráči se snaží balónek odrážet svou pálkou, hráč kterému balónek unikne prohrává.
Každou sekundu se balónek zrychluje pro zvýšení obtížnosti.

## Implementace

### Hlavní smyčka

\dotfile maindiagram.dot

Hlavní smyčka je časovaná časovačem s periodou 50ms.
V hlavní smyčce se zkontrolují stisky kláves, poté se přepočítají pozice a nakonec se snímek vykreslí.
Stisky kláves se zachytávaní asynchronně, proto je ukládám do pomocných proměnných.

Později jsem zjistil že displej nepoužívá žádný buffer, takže smazání displeje a nakreslení všeho zpět způsobovalo nepříjemné blikání.
Proto je nutné pouze přemazat původní objekt a nakreslit objekt na novou pozici.
Napsat tuto funkcionalitu do kreslicí třídy by vyžadovalo příliž mnoho času, proto jsem tuto funkcionalitu dopsal do hlavní třídy.
Hlavní třída jsi pamatuje i pozice v minulém snímku, na tyto pozice nakreslí ekvivalentní černý objekt a poté na aktuální pozici nakreslí nový objekt.

### Vykreslování

\dotfile drawqueuediagram.dot

Vykreslování probíhá asynchronně.
Příkazy se ukládají do fronty remoteDisplay::commandBuffer, která se jednou za 1ms vyprázdní pomocí serialDisplay::write_command.
Poté se v události pro příchozí data na lince zpracovává odpověď.
Pokud se na nějaký příkaz nenajde odpověď ani kladná ani záporná, předpokládá se chyba komunikace, a pocmocí prázdného příkazu (remoteDisplay::writeNop) se pokusí komunikace obnovit.
Tento příkaz se uloží do fronty příkazů a dále se pokračuje ve zpracování odpovědi.

### Vykreslení obdélníku

\dotfile drawquaddiagram.dot width=10cm

Páka nebo balónek se kreslí jako obdélníky (remoteDisplay::drawQuad).
Obdélník kreslím pomocí příkazu na vykreslení znaku se stejnou barvou znaku i pozadí.
Protože se po nakreslení znaku automaticky kurzor posune o jedno políčko doprava není nutné při kreslení řádku posouvat kurzor ručně.
Kurzor se poté přesune pouze v případě, že je potřeba kreslit na nový řádek.

### Vykreslení textu

\dotfile drawtextdiagram.dot width=10cm

Text se kreslí pomocí příkazu remoteDisplay::drawText s různou barvou znaků a pozadí.
Kreslí se od počáteční pozice na jeden řádek.



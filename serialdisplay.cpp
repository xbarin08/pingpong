#include "serialdisplay.h"
#include <QDebug>

serialDisplay::serialDisplay(QObject *parent) : remoteDisplay(parent)
{
    connect(&serial, &QSerialPort::readyRead, this,
            &serialDisplay::on_serial_ready_read);
    connect(&timeout, &QTimer::timeout, this, &serialDisplay::on_timeout);
}

QList<QSerialPortInfo> serialDisplay::availablePorts()
{
    return QSerialPortInfo::availablePorts();
}

void serialDisplay::open(const QString &portName)
{
    serial.setPortName(portName);
    serial.setBaudRate(QSerialPort::Baud115200);
    serial.setParity(QSerialPort::NoParity);
    if (!serial.open(QIODevice::ReadWrite))
    {
        qDebug() << "serial: " << serial.errorString();
        return;
    }
    timer.start();
    timeout.setInterval(1000);
}

void serialDisplay::close()
{
    serial.close();
    timer.stop();
}

void serialDisplay::on_serial_ready_read()
{
    returnBuffer.append(serial.readAll());
    if (returnBuffer.size() < 2)
    {
        return;
    }
    //    qDebug() << returnBuffer;
    unsigned numProc = 0;
    for (int i = 0; i < returnBuffer.size() - 1; ++i)
    {
        if (sentData.size() == 0)
        {
            break;
        }
        if ((i > (sentData.size() / 2)) || (i > (commandBuffer.size() / 2)))
        {
            break;
        }
        if (returnBuffer[i] == 'A')
        {
            if (returnBuffer[i + 1] == sentData[i / 2])
            {
                ++numProc;
                ++i;
                qDebug() << "ack: " << commandBuffer[i / 2].command;
            }
            else
            {
                qDebug() << "prikaz" << commandBuffer[i / 2].to_string()
                         << "nema odpoved";
                ++i;
                writeNop();
            }
        }
        else if (returnBuffer[i] == 'N')
        {
            if (returnBuffer[i + 1] == sentData[i / 2])
            {
                ++numProc;
                ++i;
                qDebug() << "nack: " << commandBuffer[i / 2].command;
            }
            else
            {
                qDebug() << "prikaz" << commandBuffer[i / 2].to_string()
                         << "nema odpoved";
                ++i;
                writeNop();
            }
        }
    }
    if (numProc > sent)
    {
        sent = 0;
    }
    else
    {
        sent -= numProc;
    }
    commandBuffer.erase(commandBuffer.begin(), commandBuffer.begin() + numProc);
    sentData.remove(0, numProc);
    returnBuffer.remove(0, numProc * 2);
    if (commandBuffer.empty())
    {
        timeout.stop();
    }
}

void serialDisplay::on_timeout()
{
    writeNop();
}

void serialDisplay::write_command()
{
    if (commandBuffer.size() < sent)
    {
        return;
    }
    for (auto it = commandBuffer.begin() + sent; it < commandBuffer.end(); ++it)
    {
        QByteArray data;
        sentData.append(it->command);
        data.append(it->command);
        data.append(it->arg1);
        data.append(it->arg2);
        data.append(0x6c);
        qDebug() << "sent: " << it->to_string();
        serial.write(data);
        ++sent;
    }
    timeout.start();
}

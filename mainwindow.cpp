#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QKeyEvent>
#include <chrono>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    for (const auto &e : serialDisplay::availablePorts())
    {
        ui->serialComboBox->addItem(e.portName());
    }

    timer.setInterval(50);
    speedUpTimer.setInterval(1000);
    connect(&timer, &QTimer::timeout, this, &MainWindow::on_timer_timeout);
    connect(&speedUpTimer, &QTimer::timeout, this,
            &MainWindow::on_speeduptimer_timeout);

    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());

    reset_pos();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    display.open(ui->serialComboBox->currentText());
    ui->serialComboBox->setEnabled(false);
    draw_reset();
    timer.start();
    ui->pushButton->setEnabled(false);
    ui->pushButton_2->setEnabled(true);
}

void MainWindow::on_pushButton_2_clicked()
{
    display.close();
    ui->serialComboBox->setEnabled(true);
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->setEnabled(false);
}

void MainWindow::on_timer_timeout()
{
    calculate_left_bat();
    calculate_right_bat();
    draw_left_bat();
    draw_right_bat();
    draw_score();

    if (spaceKey && !running)
    {
        generate_ball_direction();
        speedUpTimer.start();
        spaceKey = false;
        running = true;
    }

    draw_ball();
}

void MainWindow::on_speeduptimer_timeout()
{
    ballDirection *= 1.01f;
}

void MainWindow::keyPressEvent(QKeyEvent *key)
{
    if (key->isAutoRepeat())
    {
        return;
    }
    switch (key->key())
    {
        case Qt::Key_Q: qKey = true; break;
        case Qt::Key_A: aKey = true; break;
        case Qt::Key_P: pKey = true; break;
        case Qt::Key_L: lKey = true; break;
        case Qt::Key_Space: spaceKey = true; break;
    }
    qDebug() << key << "pressed";
}

void MainWindow::keyReleaseEvent(QKeyEvent *key)
{
    if (key->isAutoRepeat())
    {
        return;
    }
    switch (key->key())
    {
        case Qt::Key_Q: qKey = false; break;
        case Qt::Key_A: aKey = false; break;
        case Qt::Key_P: pKey = false; break;
        case Qt::Key_L: lKey = false; break;
    }
    qDebug() << key << "released";
}

void MainWindow::generate_ball_direction()
{
    float x = distribution(generator);
    ballDirection.setX(x);
    float y = 0;
    do
    {
        y = distribution(generator);
    } while (std::abs(y) > std::abs(x));
    ballDirection.setY(y);
    ballDirection.normalize();
    ballDirection *= 0.8f;
    qDebug() << ballDirection;
}

void MainWindow::reset_pos()
{
    leftBatPosition = 20;
    rightBatPosition = 20;
    ballPosition.setX(40);
    ballPosition.setY(20);
    ballDirection = QVector2D(0, 0);
    leftBatPosition_prev = leftBatPosition;
    rightBatPosition_prev = rightBatPosition;
    ballPosition_prev = ballPosition;
}

void MainWindow::draw_reset()
{
    display.clearDisplay();
    display.drawQuad(QRect(0, leftBatPosition - 3, 1, 5),
                     QColor(255, 255, 255));
    display.drawQuad(QRect(79, rightBatPosition - 3, 1, 5),
                     QColor(255, 255, 255));
    display.drawQuad(QRect(ballPosition.x(), ballPosition.y(), 1, 1),
                     QColor(255, 255, 0));
    display.drawText(QVector2D(38, 0), QColor(255, 255, 255), QColor(0, 0, 0),
                     "00:00");
}

void MainWindow::calculate_left_bat()
{
    leftBatPosition_prev = leftBatPosition;
    if (qKey && aKey) {}
    else if (qKey && (leftBatPosition > 3))
    {
        --leftBatPosition;
    }
    else if (aKey && (leftBatPosition < 38))
    {
        ++leftBatPosition;
    }
}

void MainWindow::draw_left_bat()
{
    if (leftBatPosition != leftBatPosition_prev)
    {
        clear_left_bat();
        redraw_left_bat();
    }
}

void MainWindow::clear_left_bat()
{
    display.drawQuad(QRect(0, leftBatPosition_prev - 3, 1, 5), QColor(0, 0, 0));
}

void MainWindow::redraw_left_bat()
{
    display.drawQuad(QRect(0, leftBatPosition - 3, 1, 5),
                     QColor(255, 255, 255));
}

void MainWindow::calculate_right_bat()
{
    rightBatPosition_prev = rightBatPosition;
    if (pKey && lKey) {}
    else if (pKey && (rightBatPosition > 3))
    {
        --rightBatPosition;
    }
    else if (lKey && (rightBatPosition < 38))
    {
        ++rightBatPosition;
    }
}

void MainWindow::draw_right_bat()
{
    if (rightBatPosition != rightBatPosition_prev)
    {
        clear_right_bat();
        redraw_right_bat();
    }
}

void MainWindow::clear_right_bat()
{
    display.drawQuad(QRect(79, rightBatPosition_prev - 3, 1, 5),
                     QColor(0, 0, 0));
}

void MainWindow::redraw_right_bat()
{
    display.drawQuad(QRect(79, rightBatPosition - 3, 1, 5),
                     QColor(255, 255, 255));
}

void MainWindow::draw_ball()
{
    ballPosition_prev = ballPosition;
    ballPosition += ballDirection;
    if (ballPosition.y() >= 39)
    {
        ballPosition.setY(39);
        ballDirection.setY(-ballDirection.y());
    }
    else if (ballPosition.y() <= 0)
    {
        ballPosition.setY(0);
        ballDirection.setY(-ballDirection.y());
    }
    if (std::round(ballPosition.x()) >= 79)
    {
        if ((std::round(ballPosition.y()) <= rightBatPosition + 3) &&
            (std::round(ballPosition.y()) >= rightBatPosition - 3))
        {
            ballPosition.setX(79);
            ballDirection.setX(-ballDirection.x());
        }
        else
        {
            reset_pos();
            draw_reset();
            ++leftScore;
            speedUpTimer.stop();
            running = false;
        }
    }
    else if (std::round(ballPosition.x()) <= 0)
    {
        if ((std::round(ballPosition.y()) <= leftBatPosition + 3) &&
            (std::round(ballPosition.y()) >= leftBatPosition - 3))
        {
            ballPosition.setX(0);
            ballDirection.setX(-ballDirection.x());
        }
        else
        {
            reset_pos();
            draw_reset();
            ++rightScore;
            speedUpTimer.stop();
            running = false;
        }
    }
    if (QRect(std::round(ballPosition.x()), std::round(ballPosition.y()), 1,
              1) != QRect(std::round(ballPosition_prev.x()),
                          std::round(ballPosition_prev.y()), 1, 1))
    {
        display.drawQuad(QRect(std::round(ballPosition_prev.x()),
                               std::round(ballPosition_prev.y()), 1, 1),
                         QColor(0, 0, 0));
        display.drawQuad(QRect(std::round(ballPosition.x()),
                               std::round(ballPosition.y()), 1, 1),
                         QColor(255, 255, 0));
    }
    if (ball_left_bat_intersection())
    {
        redraw_left_bat();
    }
    if (ball_right_bat_intersection())
    {
        redraw_right_bat();
    }
    if (ball_score_intersection())
    {
        redraw_score();
    }
}

void MainWindow::draw_score()
{
    string_prev = string;
    string.clear();
    string.append(QString("%1").arg(leftScore, 2, 10, QChar('0')));
    string.append(":");
    string.append(QString("%1").arg(rightScore, 2, 10, QChar('0')));
    if (string != string_prev)
    {
        clear_score();
        redraw_score();
    }
}

void MainWindow::clear_score()
{
    display.drawText(QVector2D(38, 0), QColor(0, 0, 0), QColor(0, 0, 0),
                     string_prev);
}

void MainWindow::redraw_score()
{
    display.drawText(QVector2D(38, 0), QColor(255, 255, 255), QColor(0, 0, 0),
                     string);
}

bool MainWindow::ball_right_bat_intersection()
{
    QRect rounded_ball;
    rounded_ball.setX(std::round(ballPosition_prev.x()));
    rounded_ball.setY(std::round(ballPosition_prev.y()));
    return ((rounded_ball.x() >= 79) &&
            ((rounded_ball.y() <= rightBatPosition + 3) ||
             (rounded_ball.y() >= rightBatPosition - 3)));
}

bool MainWindow::ball_left_bat_intersection()
{
    QRect rounded_ball;
    rounded_ball.setX(std::round(ballPosition_prev.x()));
    rounded_ball.setY(std::round(ballPosition_prev.y()));
    return ((rounded_ball.x() <= 0) &&
            ((rounded_ball.y() <= leftBatPosition + 3) ||
             (rounded_ball.y() >= leftBatPosition - 3)));
}

bool MainWindow::ball_score_intersection()
{
    QRect rounded_ball;
    rounded_ball.setX(std::round(ballPosition_prev.x()));
    rounded_ball.setY(std::round(ballPosition_prev.y()));
    return ((rounded_ball.y() <= 0) &&
            ((rounded_ball.x() >= 38) && (rounded_ball.x() <= 42)));
}

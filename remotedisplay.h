#ifndef REMOTEDISPLAY_H
#define REMOTEDISPLAY_H

#include <QColor>
#include <QObject>
#include <QRect>
#include <QTimer>
#include <QVector2D>

/**
 * @brief Příkaz pro displej
 */
struct displayCommand
{
    char command;
    quint8 arg1;
    quint8 arg2;
    /**
     * @brief konverze do řetězce
     * @return  řetězec
     */
    QString to_string() const;
};

/**
 * @brief vygeneruje atributy pro kreslení textu
 * @param bgcol barva pozadí
 * @param chcol barva znaku
 * @param blink blikání
 * @return bajt s atributy
 */
char setTextAtribudes(const QColor &bgcol, const QColor &chcol, bool blink);

/**
 * @brief vygeneruje atributy pro kurzor
 * @param col barva
 * @param mode mód velký nebo malý
 * @param blink blikání
 * @param visible viditelný
 * @return bajt s atributy
 */
char setCursorAtributes(const QColor &col, bool mode, bool blink, bool visible);

/**
 * @brief základní třída pro komunikaci s displejem
 * @details tato třída implementuje rozhraní, její rozšíření ::serialDisplay
 * komunikuje po sériové lince, lze dodělat rozšíření pro komunikaci po TCP.
 *
 * Tato třída ukládá příkazy do fronty a tu posílá jednou za 1ms.
 */
class remoteDisplay : public QObject
{
    Q_OBJECT
public:
    explicit remoteDisplay(QObject *parent = nullptr);

    /**
     * @brief otevření portu
     * @param port název sériového portu
     * @details Parametr by byl v případě TCP verze použit jinak
     */
    virtual void open(const QString &port) = 0;

    /**
     * @brief uzavření portu
     */
    virtual void close() = 0;

    /**
     * @brief vymazání displeje
     * @details pošle příkaz pro vymazání displeje
     */
    virtual void clearDisplay();

    /**
     * @brief vykreslení obdélníku
     * @param rect rozměry obdélníku
     * @param color barva obdélníku
     * @details Vzužije příkaz pro vypsání znaku a nastaví barvu pozadí i barvu
     * znaku na stejnou hodnotu. protože se po napsání znaku automaticky posune
     * kurzor o jednu pozici, volá se remoteDisplay::setCursor pouze při
     * kreslení dalšího řádku.
     */
    virtual void drawQuad(const QRect &rect, const QColor &color);

    /**
     * @brief Nastaví kurzor
     * @param pos pozice kurzoru
     * @param cursorAtributes viz ::setCursorAtributes
     */
    virtual void setCursor(QVector2D pos, char cursorAtributes);

    /**
     * @brief vykreslí text
     * @param pos počáteční pozice textu
     * @param textColor barva textu
     * @param bgColor barva pozadí
     * @param string text
     * @details vykreslí text od pozice pos
     */
    virtual void drawText(const QVector2D &pos, const QColor &textColor,
                          const QColor &bgColor, const QString &string);

public slots:

    /**
     * @brief Událost vyprazdňování fronty
     */
    void on_timer_timeout();

protected:
    /**
     * @brief pošle prázdný příkaz
     */
    virtual void writeNop();

    /**
     * @brief pošle vykreslení znaku
     * @param c znak
     * @param textAtribute atributy viz ::setTextAtribudes
     */
    virtual void writeChar(char c, char textAtribute);

    /**
     * @brief nastaví atributy kurzoru
     * @param cursorAtributes atributy viz::setCursorAtributes
     */
    virtual void writeCursorAtributes(char cursorAtributes);

    /**
     * @brief nastaví pozici kurzoru
     * @param pos pozice
     */
    virtual void writePos(QVector2D pos);

    /**
     * @brief vyprázdnění fronty
     */
    virtual void write_command() = 0;

    /**
     * @brief fronta příkazů
     */
    QList<displayCommand> commandBuffer;

    /**
     * @brief časovač pro vyprazdňování fronty
     */
    QTimer timer;
};

#endif // REMOTEDISPLAY_H

#include "remotedisplay.h"
#include <QDebug>

remoteDisplay::remoteDisplay(QObject *parent) : QObject(parent)
{
    timer.setInterval(1);
    connect(&timer, &QTimer::timeout, this, &remoteDisplay::on_timer_timeout);
}

void remoteDisplay::clearDisplay()
{
    writeChar(' ', setTextAtribudes(QColor(0, 0, 0), QColor(0, 0, 0), false));
    displayCommand cmd;
    cmd.command = 'e';
    commandBuffer.append(cmd);
}

void remoteDisplay::drawQuad(const QRect &rect, const QColor &color)
{
    char c = setTextAtribudes(color, color, false);
    for (int i = 0; i < rect.height(); ++i)
    {
        writePos(QVector2D(rect.left(), rect.top() + i));
        for (int j = 0; j < rect.width(); ++j)
        {
            writeChar('T', c);
        }
    }
}

void remoteDisplay::setCursor(QVector2D pos, char cursorAtributes)
{
    writeCursorAtributes(cursorAtributes);
    writePos(pos);
}

void remoteDisplay::drawText(const QVector2D &pos, const QColor &textColor,
                             const QColor &bgColor, const QString &string)
{
    writePos(pos);
    char c = setTextAtribudes(bgColor, textColor, false);
    for (const auto e : string)
    {
        writeChar(e.toLatin1(), c);
    }
}

void remoteDisplay::on_timer_timeout()
{
    write_command();
}

void remoteDisplay::writeNop()
{
    if (commandBuffer.back().command != 'n')
    {
        displayCommand cmd;
        cmd.command = 'n';
        commandBuffer.append(cmd);
    }
    qDebug() << "writing nop";
}

void remoteDisplay::writeChar(char c, char textAtribute)
{
    displayCommand cmd;
    cmd.command = 'c';
    cmd.arg1 = c;
    cmd.arg2 = quint8(textAtribute);
    commandBuffer.append(cmd);
}

void remoteDisplay::writeCursorAtributes(char cursorAtributes)
{
    displayCommand cmd;
    cmd.command = 's';
    cmd.arg1 = quint8(cursorAtributes);
    commandBuffer.append(cmd);
}

void remoteDisplay::writePos(QVector2D pos)
{
    if ((pos.x() > 79) || (pos.x() < 0) || (pos.y() > 39) || (pos.y() < 0))
    {
        throw std::runtime_error("wrong cursor position");
    }
    displayCommand cmd;
    cmd.command = 'p';
    cmd.arg1 = quint8(pos.x());
    cmd.arg2 = quint8(pos.y());
    commandBuffer.append(cmd);
}

char setTextAtribudes(const QColor &bgcol, const QColor &chcol, bool blink)
{
    char c = 0;
    if (blink)
    {
        c |= 0x80;
    }
    if (chcol.blue() > 127)
    {
        c |= 0x40;
    }
    if (chcol.green() > 127)
    {
        c |= 0x20;
    }
    if (chcol.red() > 127)
    {
        c |= 0x10;
    }
    if (bgcol.blue() > 127)
    {
        c |= 0x4;
    }
    if (bgcol.green() > 127)
    {
        c |= 0x2;
    }
    if (bgcol.red() > 127)
    {
        c |= 0x1;
    }
    return c;
}

char setCursorAtributes(const QColor &col, bool mode, bool blink, bool visible)
{
    char c = 0;
    if (visible)
    {
        c |= 0x40;
    }
    if (blink)
    {
        c |= 0x20;
    }
    if (mode)
    {
        c |= 0x10;
    }
    if (col.blue() > 127)
    {
        c |= 0x4;
    }
    if (col.green() > 127)
    {
        c |= 0x2;
    }
    if (col.red() > 127)
    {
        c |= 0x1;
    }
    return c;
}

QString displayCommand::to_string() const
{
    QString tmp;
    switch (command)
    {
        case 'c':
            tmp.append("CHAR ");
            tmp.append(char(arg1));
            tmp.append(" ");
            tmp.append(arg2);
            break;
        case 'p':
            tmp.append("POS ");
            tmp.append(arg1);
            tmp.append(" ");
            tmp.append(arg2);
            break;
        case 's':
            tmp.append("SET ");
            tmp.append(arg1);
            tmp.append(" ");
            tmp.append(arg2);
            break;
        case 'n': tmp.append("NOP"); break;
        case 'g': tmp.append("GETSTATUS"); break;
        case 'e': tmp.append("ERSSCR"); break;
        default: break;
    }
    return tmp;
}

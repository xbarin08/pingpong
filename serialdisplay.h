#ifndef SERIALDISPLAY_H
#define SERIALDISPLAY_H

#include "remotedisplay.h"
#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>

/**
 * @brief rozhraní pro komunikaci po sériové lince
 * @details rozšíření ::remoteDisplay
 */
class serialDisplay : public remoteDisplay
{
    Q_OBJECT
public:
    explicit serialDisplay(QObject *parent = nullptr);

    /**
     * @brief seznam dostupných sériových portů
     * @return seznam dostupných sériových portů
     */
    static QList<QSerialPortInfo> availablePorts();

    /**
     * @brief otevře port
     * @param portName jméno portu
     */
    void open(const QString &portName) override;

    /**
     * @brief zavře port
     */
    void close() override;

private slots:

    /**
     * @brief Událost pro dostupné bajty na sériovém portu
     */
    void on_serial_ready_read();

    /**
     * @brief Událost pro vypršení komunikace
     * @details Pokud se všechny příkazy do 1s nepotvrdí vyvolá se tato událost,
     * která vyšle prázdný příkaz a tak se pokusí obnovit komunikaci.
     */
    void on_timeout();

protected:
    /**
     * @brief vyprázdnění fronty příkazů
     */
    void write_command() override;

    /**
     * @brief Objekt sériového portu
     */
    QSerialPort serial;

    /**
     * @brief Buffer s přijatými bajty
     */
    QByteArray returnBuffer;

    /**
     * @brief Buffer s odeslanými bajty
     */
    QByteArray sentData;

    /**
     * @brief časovač pro vypršení komunikace
     * @details vyvolá serialDisplay::on_timeout
     */
    QTimer timeout;

    /**
     * @brief počet poslaných nepotvrzených příkazů
     */
    size_t sent = 0;
};

#endif // SERIALDISPLAY_H
